FROM openjdk:15
ADD target/SQLtoRestJan-0.0.1-SNAPSHOT.jar SQLtoRestJan-0.0.1-SNAPSHOT.jar
ENTRYPOINT [ "java","-jar", "SQLtoRestJan-0.0.1-SNAPSHOT.jar"]