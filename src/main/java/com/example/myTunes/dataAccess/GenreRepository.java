package com.example.myTunes.dataAccess;

import com.example.myTunes.logging.LogToConsole;
import com.example.myTunes.models.Genre;
import com.example.myTunes.models.Track;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class GenreRepository {

    private LogToConsole logger = new LogToConsole();
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    //Method returns 5 random genres presented on the homepage.
    // these are 5 new random ones every time the site is refreshed.
    public ArrayList<Genre> getFiveGenres(){
        ArrayList<Genre> genres = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");
            //query to get 5 random.
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT [Name] FROM Genre ORDER BY RANDOM() LIMIT 5");

            ResultSet resultSet = preparedStatement.executeQuery();
            //adds to the resultlist while there is more to add.
            while (resultSet.next()) {
                genres.add(
                        new Genre(
                                resultSet.getString("Name")

                        ));
            }
            logger.log("Select 5 genres successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return genres;
    }
}
