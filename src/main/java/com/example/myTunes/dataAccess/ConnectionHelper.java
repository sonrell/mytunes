package com.example.myTunes.dataAccess;

public class ConnectionHelper {
    // field to make the connectionstring static, need only to write it once.
    public static final String CONNECTION_URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";

}
