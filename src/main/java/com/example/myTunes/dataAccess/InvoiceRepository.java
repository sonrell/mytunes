package com.example.myTunes.dataAccess;

import com.example.myTunes.logging.LogToConsole;
import com.example.myTunes.models.HighestSpenders;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;

public class InvoiceRepository {

    private LogToConsole logger = new LogToConsole();

    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    //method to retrive the highest spendes by descending order based on invoices.
    public ArrayList<HighestSpenders> getHighSpenders(){
        ArrayList<HighestSpenders> customersSpenders = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerId,Total FROM invoice ORDER BY Total DESC");

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customersSpenders.add(
                        new HighestSpenders(
                                resultSet.getString("CustomerId"),
                                resultSet.getString("Total")
                        ));
            }
            logger.log("Select all customers successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return customersSpenders;
    }
}
