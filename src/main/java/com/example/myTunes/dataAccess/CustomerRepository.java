package com.example.myTunes.dataAccess;

import com.example.myTunes.logging.LogToConsole;
import com.example.myTunes.models.CountryCount;
import com.example.myTunes.models.Customer;
import com.example.myTunes.models.Genre;

import java.util.Comparator;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

import static java.lang.Integer.parseInt;

public class CustomerRepository {

    private LogToConsole logger = new LogToConsole();

    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;


    // Method to retrieve a given customers favouirte genre. Meaning the genre of which the customer has most invoices connected to.
    public ArrayList<Genre> getFavouriteGenre(Customer customer){
        String id = customer.getCustomerId();
        ArrayList<Genre> genres = new ArrayList<>();
        try{

            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            //query to get the information needed. INNER JOINS from genre all the way to customer to reach the customer id to match the given customers id.
            PreparedStatement preparedStatement =
                     conn.prepareStatement("SELECT Genre.Name , COUNT(*) AS rowcount FROM Genre INNER JOIN Track ON genre.genreId = track.genreId INNER JOIN InvoiceLine ON Track.Trackid = InvoiceLine.TrackId INNER JOIN Invoice ON InvoiceLine.InvoiceId = Invoice.InvoiceId  WHERE Invoice.CustomerId = ? GROUP BY Genre.name ORDER BY COUNT (*) DESC ");
            preparedStatement.setString(1,id);

            ResultSet resultSet = preparedStatement.executeQuery();
            int currMax= 0;
            while (resultSet.next()) {

                int fromSql=parseInt(resultSet.getString("rowcount"));
                if (fromSql>=currMax){
                    currMax=fromSql;
                    genres.add(
                            new Genre(
                                    resultSet.getString("Name")
                            )
                    );
                }
            }
            logger.log("Select favourite genre was successfull");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        //returns a collection of Genre objects recieved from the query.
        return genres;
    }

    //Method to retrieve all customers in the database.
    //Retrieves CustomerID, firstname, lastname, country, postalcode, phone and email.
    public ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> customers = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerId,FirstName, LastName,Country, PostalCode, Phone, Email FROM customer");
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getString("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Email")
                        ));
            }
            logger.log("Select all customers successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        //returns the a collection of the information retrieved from query.
        return customers;
    }

    //Method to add a customer to the database.
    public Boolean addCustomer(Customer customer){
        Boolean success = false;
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            //query to set the values.
            PreparedStatement preparedStatement =
                    conn.prepareStatement("INSERT INTO customer(customerId,firstName,lastName,phone, country, postalCode,email) VALUES(?,?,?,?,?,?,?)");
            preparedStatement.setString(1,customer.getCustomerId());
            preparedStatement.setString(2,customer.getFirstName());
            preparedStatement.setString(3,customer.getLastName());
            preparedStatement.setString(4,customer.getPhone());
            preparedStatement.setString(5,customer.getCountry());
            preparedStatement.setString(6,customer.getPostalCode());
            preparedStatement.setString(7,customer.getEmail());

            int result = preparedStatement.executeUpdate();
            success = (result != 0);
            logger.log("Add customer successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return success;
    }

    //method to return a collection of customers by their country. CountryCount objects which stores countryname and count.
    //returns the coutry with highest number of occurances.
    public Collection<CountryCount> customerByCountry(){

        Collection<CountryCount> result = new ArrayList<>();
        Collection<Customer> allCustomers = getAllCustomers();
        List<Customer> allCustList = new ArrayList<>();

        for(Customer cust : allCustomers){
            allCustList.add(cust);
        }

        //A hashmap to store the name of the country as key and count of occurance as the value.
        Map<String, Integer> customerHash = new LinkedHashMap<>();

        for (int i = 0; i < allCustList.size(); i++) {
            String country = allCustList.get(i).getCountry();

            //checks if the key already contains in the hashmap to avoid pointing at null.
            // if it do have it it increases the count by 1. If it doesnt it sets it to 1.
            if(customerHash.containsKey(country)){
                customerHash.put(country, customerHash.get(country) + 1);
            }else{
                customerHash.put(allCustList.get(i).getCountry(),1);
            }
        }

        // Craetes a new hashmap to sort it by descening order.
        Map<String,Integer> customerHashSorted = new LinkedHashMap<>();
        customerHash.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEachOrdered(x->customerHashSorted.put(x.getKey(),x.getValue()));

        //fills the result-collection with countryname and count from hashmap.
        Iterator it = customerHashSorted.entrySet().iterator();
        while(it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            CountryCount cc = new CountryCount(pair.getKey().toString(),(int) pair.getValue());
            result.add(cc);
        }
        return result;
    }

    //method to update a given customers information.
    public Boolean updateCustomer(Customer customer){
        Boolean success = false;
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            //query to set the new values.
            PreparedStatement preparedStatement =
                    conn.prepareStatement("UPDATE customer SET customerId = ?, firstName = ?, lastName = ?, phone = ?,country =?,postalCode=?, email=? WHERE customerId=?");
            preparedStatement.setString(1,customer.getCustomerId());
            preparedStatement.setString(2,customer.getFirstName());
            preparedStatement.setString(3,customer.getLastName());
            preparedStatement.setString(4,customer.getPhone());
            preparedStatement.setString(5,customer.getCountry());
            preparedStatement.setString(6,customer.getPostalCode());
            preparedStatement.setString(7,customer.getEmail());
            preparedStatement.setString(8,customer.getCustomerId());


            int result = preparedStatement.executeUpdate();
            success = (result != 0);
            logger.log("Update customer successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return success;
    }
}
