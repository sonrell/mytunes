package com.example.myTunes.dataAccess;


import com.example.myTunes.logging.LogToConsole;
import com.example.myTunes.models.Artist;
import com.example.myTunes.models.Customer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ArtistRepository {

    private LogToConsole logger = new LogToConsole();
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    //Method returns 5 random artist presented on the homepage.
    // these are 5 new random ones every time the site is refreshed.
    public ArrayList<Artist> getFiveArtists(){
        ArrayList<Artist> artists = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");
            //Query to get 5 random artists.
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT [Name] FROM Artist ORDER BY RANDOM() LIMIT 5");

            ResultSet resultSet = preparedStatement.executeQuery();
            //adds to the resultlist while there is more to add.
            while (resultSet.next()) {
                artists.add(
                        new Artist(
                                resultSet.getString("name")
                        ));
            }
            logger.log("Select 5 artists successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return artists;
    }
}
