package com.example.myTunes.dataAccess;

import com.example.myTunes.logging.LogToConsole;
import com.example.myTunes.models.Customer;
import com.example.myTunes.models.Track;
import com.example.myTunes.models.TrackFound;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class TrackRepository {


    private LogToConsole logger = new LogToConsole();
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    //Method returns 5 random tracks presented on the homepage.
    // these are 5 new random ones every time the site is refreshed.
    public ArrayList<Track> getFiveTracks(){
        ArrayList<Track> tracks = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            //query to get 5 random.
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT [Name] FROM Track ORDER BY RANDOM() LIMIT 5");

            ResultSet resultSet = preparedStatement.executeQuery();

            //adds to the resultlist while there is more to add.
            while (resultSet.next()) {
                tracks.add(
                        new Track(
                                resultSet.getString("Name")

                        ));
            }
            logger.log("get 5 random tracks successfull");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return tracks;
    }

    //Method to search for a given track. Takes name, which is input from user at index.html form.
    //uses the information retrieved from query to return a list of TrackFound objects with its corresponding attributes.
    public ArrayList<TrackFound> findTrack(String name){
        ArrayList<TrackFound> tracksFound = new ArrayList<>();
        try{
            //Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            //creates a string from input "name" to use it in the SQL query.
            StringBuilder sb = new StringBuilder(name);
            sb.append('%');
            sb.insert(0, '%');

            // query uses the search term to retrieve artistname, trackname, genrename and album name, based on what is searched for.
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Track.Name AS trackName, Artist.name AS artistName,Album.title AS albumName, Genre.name AS genreName from Genre   INNER JOIN Track ON Genre.genreId=Track.genreId  INNER JOIN Album ON Track.albumId=Album.albumId INNER JOIN Artist ON Album.artistId=Artist.artistId WHERE Track.name LIKE ?");

            preparedStatement.setString(1,sb.toString());
            ResultSet resultSet = preparedStatement.executeQuery();
            //adds to the resultlist while there is more to add.
            while (resultSet.next()) {
                tracksFound.add(
                        new TrackFound(
                                resultSet.getString("trackName"),
                                resultSet.getString("artistName"),
                                resultSet.getString("albumName"),
                                resultSet.getString("genreName")
                        ));
            }
            logger.log("search successfull");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        for(TrackFound track : tracksFound) {
            System.out.println(track.getName());
        }
        return tracksFound;
    }
}
