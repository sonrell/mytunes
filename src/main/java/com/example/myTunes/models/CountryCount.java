package com.example.myTunes.models;


public class CountryCount {

    //Model to hold info about how many occurances there is of customers from a country.

    private int count;
    private String country;

    public CountryCount(String country, int count) {
        this.count = count;
        this.country = country;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
