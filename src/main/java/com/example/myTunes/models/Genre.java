package com.example.myTunes.models;

public class Genre {

    //Genremodel used to hold and set info about genre.

    private String name;

    public Genre(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
