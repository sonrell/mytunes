package com.example.myTunes.models;

public class Artist {

    //Artistmodel used to hold and set info about artists.

    private String name;

    public Artist(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
