package com.example.myTunes.models;

public class Track {

    //Trackmodel used to hold and set info about tracks.

    private String name;

    public Track(String name) {
        this.name = name;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
