package com.example.myTunes.models;

public class HighestSpenders {

    //HighestSpendermodel used to hold and set info about highest spenders.

    String customerId;
    String total;


    public HighestSpenders(String customerId, String total) {
        this.customerId = customerId;
        this.total = total;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }



}
