package com.example.myTunes.logging;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LogToConsole {

    //logs to console with status of calls
    public void log(String message){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + " : " + message);
    }

}
