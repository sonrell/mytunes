package com.example.myTunes.controllers;

import com.example.myTunes.dataAccess.ArtistRepository;
import com.example.myTunes.dataAccess.GenreRepository;
import com.example.myTunes.dataAccess.TrackRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class MusicController {

    TrackRepository trap = new TrackRepository();
    GenreRepository grep = new GenreRepository();
    ArtistRepository arep = new ArtistRepository();

    //Method to call the index template with 5 random tracks, 5 random genres and 5 random artists.
    //These methods are in their respective repositories. That is, TrackRepo, GenreRepo, ArtistRepo.
    //For instance, when getFiveTracks are called, it returns 5 random tracks which are set to "tracks" attribute.
    @GetMapping(value = "/")
    public String getMusic(Model model){
        model.addAttribute("tracks", trap.getFiveTracks());
        model.addAttribute("genres", grep.getFiveGenres());
        model.addAttribute("artists", arep.getFiveArtists());
        return "index";
    }

    @GetMapping(value = "/search")
    public String getSpecificTrack(
            /*RequestParam from the submit form in the views.*/
            @RequestParam(value = "name",required = false) String name,
            Model model
    ){
        //calls the findTrack method from trackrepo with the input parameter and adds the attribute.
        //returns list of TrackFound objects to be displayed in searchview.
        model.addAttribute("tracksFound", trap.findTrack(name));
        return "searchview";
    }
}
