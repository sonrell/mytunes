package com.example.myTunes.controllers;

import com.example.myTunes.dataAccess.InvoiceRepository;
import com.example.myTunes.models.HighestSpenders;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class InvoiceController {

    private InvoiceRepository invoiceRepository = new InvoiceRepository();

    //Returns list of customers by their total invoices. Highest first.
    @RequestMapping(value = "/api/invoices", method = RequestMethod.GET)
    public ArrayList<HighestSpenders> getAllInvoices() {
        return invoiceRepository.getHighSpenders();
    }

}

