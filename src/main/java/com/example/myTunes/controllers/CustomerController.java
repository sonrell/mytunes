package com.example.myTunes.controllers;

import com.example.myTunes.dataAccess.CustomerRepository;
import com.example.myTunes.models.CountryCount;
import com.example.myTunes.models.Customer;
import com.example.myTunes.models.Genre;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;

@RestController
public class CustomerController {

    private CustomerRepository customerRepository = new CustomerRepository();

    //Retrieves a list of all customers. Calls getAllCustomers from the CustomerRepository class.
    @RequestMapping(value="/api/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomers(){
        return customerRepository.getAllCustomers();
    }


    //This adds a new customer.
    // It takes the new customer from the body of the request.
    @RequestMapping(value = "api/customers", method = RequestMethod.POST)
    public Boolean addNewCustomer(@RequestBody Customer customer){
        return customerRepository.addCustomer(customer);
    }

    //Functionality for updating an existing customer. Takes an id.
    @RequestMapping(value = "api/customers/{id}", method = RequestMethod.PUT)
    public Boolean updateExistingCustomer(@PathVariable String id, @RequestBody Customer customer){
        return customerRepository.updateCustomer(customer);
    }

    // Retrieves a collection of customer by country. The country wiht most customers are returned first etc.
    @RequestMapping(value="/api/customers/countrycount", method = RequestMethod.GET)
    public Collection<CountryCount> customerByCountry(){
        return customerRepository.customerByCountry();
    }

    //Takes a customer and returns its favourite genre, meaning most invoices connected to that genre.
    @RequestMapping(value="/api/customers/favouritegenre", method = RequestMethod.GET)
    public Collection<Genre> getFavouriteGenres(@RequestBody Customer customer){
        return customerRepository.getFavouriteGenre(customer);
    }


}
