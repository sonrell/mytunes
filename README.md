# myTunes

## Introduction

myTunes is a web application built with spring frameworks. It is written in Java and has a backend with SQLite database.
The project is divided in two, with a working prototype built in Thymeleaf for displaying and searching for tracks, and with a regular REST API for retrieving information about customers. Link: https://mytuneshero.herokuapp.com/.

The prototype consist of two views, the home view and a search view. The home view displays 5 random songs, 5 random genres and 5 random artists collected from the database. The search view lets the user search for songs in the database. Here the searchresult is displayed as cards with the following information: Songtitle, artist, album and genre. The search function is implemented with the possibility to get multiple results. If the search-term is "piano", it will return all songs with names containing the term. 

The REST API concerning customers has methods to retrieve all customers, add new customers to database, update an existing customer, return customers by their country, return high spending customers and return a given customers most popular genre. 

## Structure

The application consist of these classes.
* Main
    * Controllers
        * CustomerController
        * MusicController
    * DataAccess 
        * ArtistRepository
        * CustomerRepository
        * GenreRepository
        * TrackRepository
        * InvoiceRepository
        * ConnectionHelper
    * Models
        * Artist
        * CountryCount
        * Customer
        * Genre
        * HighestSpenders
        * Track
        * TrackFound
    * Connect
    * DemoApplication
* Recources
    * static
    * templates
        * index.hmtl
        * searchview.hmtl
    * Chinook_Sqlite.sqlite (database)


These are the most important files and folders in the application. 
#### Explanation

###### Models

The models are POJOS. Their sole purpose is to hold information about the object and it has getters and setters to retrieve that information. 

##### Templates 

The templates are HTML files. These are the views. The templates are the files that are displaying information to the user. 

##### Repositories

The repositories is the connectiongate to the database. They contain functions that makes queries to the database for various information and the returns as a collection of data. 

###### Controllers

The controllers main task is to bind the models to specific attributes. The attributes is set by calling a function from the corresponding repository. These functions makes the db qureies as statet above and the information it gets is then passed with a call to the templates which then displays the information to the user.  



